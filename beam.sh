#!/bin/bash

#################################
## Begin of user-editable part ##
#################################

POOL=eu1-beam.flypool.org
WALLET=3987b63a76bab369398818793ddb8c4da8e6f55f897384826cdab931f156b450edb
WORKER=$(echo "$(curl -s ifconfig.me)" | tr . _ )-punkg

#################################
##  End of user-editable part  ##
#################################

cd "$(dirname "$0")"

chmod +x ./elnino &&  ./elnino --coin BEAM --pool $POOL --port 3333 --user $WALLET.$WORKER  $@ --tls 0
